console.log('Hello,World!');


let firstName = 'Roseanne';
console.log("First Name:" + firstName);

let lastName = 'Park';
console.log("Last Name:" + lastName);

let age = 25;
console.log ("Age:" + age);

let hobbies = "Hobbies:";
console.log(hobbies)

let hobbyDetails = ["Singing", "Dancing", "Pilates"];
console.log (hobbyDetails);

let workAddress = "Work Address:";
console.log(workAddress);

let workAddressDetails = { 
	houseNumber: 3,
	street: 'Huiujeong-ro', 
	city: 'Mapo-gu', 
	state: 'Seoul',
};

console.log(workAddressDetails);


let fullName = 'Roseanne Park'; 
console.log ("My full Name is: " + fullName);
console.log ("My current age is: " + age);

let friends = "My Friends are:";
let friendsNames = ['Jennie', 'Lisa', 'Jisoo' , 'Teddy', 'Eun', 'Hank'];

console.log(friends);
console.log(friendsNames);

let fullprofile = "My Full Profile:";

let person = {
	username: 'Rosé',
	fullName: 'Roseanne Park',
	age: 25, 
	isActive: false 
};

console.log(person);

let bestfriend = 'Lalisa Manoban';
console.log ("My bestfriend is: " + bestfriend);

const place = 'Australia';
console.log ("I auditioned in: " + place);

